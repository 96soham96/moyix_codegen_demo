#!/bin/bash
mkdir -p triton_server
curl -L -O https://huggingface.co/moyix/codegen-350M-mono-gptj/resolve/main/codegen-350M-mono-1gpu.tar.zst
tar --use-compress-program=unzstd -xvf codegen-350M-mono-1gpu.tar.zst -C triton_server
wget -O triton_server/codegen-350M-mono-1gpu/fastertransformer/config.pbtxt https://raw.githubusercontent.com/fauxpilot/fauxpilot/main/converter/models/codegen-350M-mono-1gpu/fastertransformer/config.pbtxt
