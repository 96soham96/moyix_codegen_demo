# moyix_codegen_demo

To download model files, please first run the `setup.sh` script

After that to start service, please run `docker compose up`

To view API docs, go to `0.0.0.0:5000`

To view plotly dash `0.0.0.0:8050`
