from dash import Dash, html, dcc, callback, Output, Input, State
import plotly.express as px
import pandas as pd
import requests

df = pd.read_csv('https://raw.githubusercontent.com/plotly/datasets/master/gapminder_unfiltered.csv')

app = Dash(__name__)

app.layout = html.Div([
    html.H1(children='Moyix GPTJ Code Gen Model Demo', style={'textAlign':'center'}),
    html.Div([dcc.Input(id='prompt', placeholder='Enter Prompt', type='text')]),
    html.Div([html.Button('Submit', id='button')]),
    html.Div(id='output-content', style={'whiteSpace': 'pre-line'})
])

@app.callback(
    Output(component_id='output-content', component_property='children'),
    Input('button', 'n_clicks'),
    [State(component_id='prompt', component_property='value')]
)
def update_output(_, prompt):
    print('here')
    payload={
          "model": "fastertransformer",
          "prompt": prompt,
          "suffix": "string",
          "max_tokens": 500,
          "temperature": 0.5,
          "top_p": 1,
          "n": 1,
          "stream": False,
          "logprobs": 0,
          "echo": True,
          "stop": "string",
          "presence_penalty": 0,
          "frequency_penalty": 1,
          "best_of": 1,
          "logit_bias": {},
          "user": "string"
        }
    url='http://codegen_client:5000/v1/completions'
    x = requests.post(url, json = payload)
    output=x.json()['choices'][0]['text']

    return output

if __name__ == '__main__':
    app.run_server(host='0.0.0.0', port='8050', debug=False)
