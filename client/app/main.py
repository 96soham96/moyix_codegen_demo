import logging
import os
from typing import Optional, Union

import uvicorn
from fastapi import FastAPI, Request, Response
from fastapi.responses import JSONResponse
from pydantic import BaseModel, constr

from model import CodeGenModel

codegen=CodeGenModel()

class CodeGenInput(BaseModel):
    model: str = "fastertransformer"
    prompt: Optional[str]
    suffix: Optional[str]
    max_tokens: Optional[int] = 16
    temperature: Optional[float] = 0.6
    top_p: Optional[float] = 1.0
    n: Optional[int] = 1
    stream: Optional[bool]
    logprobs: Optional[int] = None
    echo: Optional[bool]
    stop: Optional[Union[str, list]]
    presence_penalty: Optional[float] = 0
    frequency_penalty: Optional[float] = 1
    best_of: Optional[int] = 1
    logit_bias: Optional[dict]
    user: Optional[str]

app = FastAPI(docs_url="/")

@app.post("/v1/completions")
async def completions(data: CodeGenInput):
    data = data.dict()
    try:
        content = codegen(data=data)
    except codegen.TokensExceedsMaximum as E:
        raise FauxPilotException(
            message=str(E),
            error_type="invalid_request_error",
            param=None,
            code=None,
        )

    return Response(
                status_code=200,
                content=content,
                media_type="application/json"
            )

if __name__ == "__main__":
    uvicorn.run("app:app", host="0.0.0.0", port=5000)
